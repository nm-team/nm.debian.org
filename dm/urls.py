# coding: utf-8

from django.urls import path
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    path('', RedirectView.as_view(url="/", permanent=True), name="dm_index"),
    path('claim', views.Claim.as_view(), name="dm_claim"),
    path('claim/confirm/<token>', views.ClaimConfirm.as_view(), name="dm_claim_confirm"),
]
